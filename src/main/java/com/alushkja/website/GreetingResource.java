package com.alushkja.website;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/greet")
public class GreetingResource {

    @GET
    public Response greet() {
        return Response.ok("Aldo Lushkja website").build();
    }

}

package com.alushkja.website;

import io.quarkus.qute.Location;
import io.quarkus.qute.Template;
import io.quarkus.qute.TemplateInstance;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Path("/")
public class HomeTemplate {

    @Location("index.html")
    Template template;


    @GET
    @Produces(MediaType.TEXT_HTML)
    public TemplateInstance home() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss yyyy-MM-dd");
        return template.data("date", LocalDateTime.now().format(dateTimeFormatter));
    }
}
